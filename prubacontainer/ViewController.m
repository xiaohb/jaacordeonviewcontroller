//
//  ViewController.m
//
//  Created by Javier Alonso Gutierrez on 14/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"


#import "tresVC.h"

@interface ViewController(){
    JAAcordeonViewController *acordeon;
}

@end

@implementation ViewController
@synthesize numero;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    tresVC *uno=[[tresVC alloc] initWithNibName:@"tresVC" bundle:nil];
    tresVC *dos=[[tresVC alloc] initWithNibName:@"tresVC" bundle:nil];
    tresVC *tres=[[tresVC alloc] initWithNibName:@"tresVC" bundle:nil];
    tresVC *cuatro=[[tresVC alloc] initWithNibName:@"tresVC" bundle:nil];
    tresVC *cinco=[[tresVC alloc] initWithNibName:@"tresVC" bundle:nil];
    
    uno.title=@"VC 1";
    dos.title=@"VC 2";
    tres.title=@"VC 3";
    cuatro.title=nil;//@"VC 4";
    cinco.title=@"VC 5";
    
    CGFloat acordeonHeight=self.view.bounds.size.height-(self.numero.frame.origin.y+self.numero.frame.size.height)-20;
    
    acordeon=[[JAAcordeonViewController alloc] initWithViewControllers:uno,dos,tres,cuatro,cinco, nil];
    
    acordeon.view.frame=CGRectMake(0, self.view.bounds.size.height-acordeonHeight, self.view.bounds.size.width, acordeonHeight);
    acordeon.view.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    acordeon.delegate=self;
    //acordeon.tint=[UIColor colorWithHue:0 saturation:0 brightness:0.3 alpha:1];
    acordeon.tint=[UIColor orangeColor];
    [self.view addSubview:acordeon.view];
}

- (void)viewDidUnload
{
    [self setNumero:nil];
    [super viewDidUnload];
    acordeon=nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (IBAction)add:(id)sender {
    tresVC *uno=[[tresVC alloc] initWithNibName:@"tresVC" bundle:nil];
    uno.title=[NSString stringWithFormat:@"nuevo %@",self.numero.text];
    [acordeon insertViewController:uno atIndex:self.numero.text.intValue animated:YES];
}

- (IBAction)remove:(id)sender {
    [acordeon removeViewControllerAtIndex:self.numero.text.intValue animated:YES];
}

- (IBAction)cambiar:(id)sender {
    if (acordeon.direccion==AcordeonDireccionHorizontal) {
        [acordeon setDireccion:AcordeonDireccionVertical animated:NO];
    }else{
        [acordeon setDireccion:AcordeonDireccionHorizontal animated:NO];
    }
}
/*-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        CGFloat acordeonHeight=self.view.bounds.size.height-(self.numero.frame.origin.y+self.numero.frame.size.height);
        acordeon.view.frame=CGRectMake(0, self.view.bounds.size.height-acordeonHeight, self.view.bounds.size.width, acordeonHeight);
        acordeon.view.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
        //[acordeon setDireccion:AcordeonDireccionHorizontal animated:NO];
    }else{
        acordeon.view.frame=CGRectMake(0, 0, 300, self.view.bounds.size.height);
        acordeon.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin;
        //[acordeon setDireccion:AcordeonDireccionVertical animated:NO];
    }
}*/

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)jaacordeonViewController:(JAAcordeonViewController *)acordeonViewController shouldActivateViewController:(UIViewController *)viewController{
    return viewController.title!=nil;
}

-(UIImage *)jaacordeonViewController:(JAAcordeonViewController *)acordeonViewController imageForViewController:(UIViewController *)viewController{
    return [UIImage imageNamed:@"gold-star.png"];
}

@end
