//
//  AcordeonViewController.m
//
//  Created by Javier Alonso Gutierrez on 14/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "JAAcordeonViewController.h"
#import <QuartzCore/QuartzCore.h>

#define ACORDEON_TITLE_SIZE 40
#define ACORDEON_VIEW_MINSIZE 50
#define ACORDEON_TITLE_MARGIN 10
#define ACORDEON_TITLE_MARGIN_2 20
#define ACORDEON_ANIMATION_DURATION 0.3
#define ACORDEON_TITLE_IMAGE_SIDE 30

@interface JAAcordeonViewController()<UIGestureRecognizerDelegate>{
    NSMutableArray *_controllers,*_titleViews;
    UIView *_currentView;
    int _activeTab;
    AcordeonDireccion _direccion;
    BOOL _animating;
    UISwipeGestureRecognizer *_swipeIncreaseGR, *_swipeDecreaseGR;
}

-(void)activateViewController:(UIViewController *)viewController animated:(BOOL)animated;
-(void)tappedTitleView:(UITapGestureRecognizer *)tapGR;
-(void)layoutForDirection:(AcordeonDireccion)direccion;
-(UIImage *)imageForViewController:(UIViewController *)viewController;
-(void)swiped:(UISwipeGestureRecognizer *)swipeGR;

@end

@implementation JAAcordeonViewController

@synthesize delegate=_delegate;
@synthesize tint=_tint;

-(void)setDelegate:(id<JAAcordeonViewControllerDelegate>)delegate{
    if (delegate!=_delegate) {
        _delegate=delegate;
        if ([self isViewLoaded]) {
            for (int i=0; i< [_titleViews count]; i++) {
                JAAcordeonBar *bar =[_titleViews objectAtIndex:i];
                bar.item.image=[self imageForViewController:[_controllers objectAtIndex:i]];
            }
        }
    }
}

-(void)setTint:(UIColor *)tint{
    if (tint!=_tint) {
        _tint=tint;
        if ([self isViewLoaded]) {
            for (JAAcordeonBar *bar in _titleViews) {
                bar.tint=_tint;
            }
        }
    }
}

-(void)layoutForDirection:(AcordeonDireccion)direccion{
    if (direccion==AcordeonDireccionHorizontal) {
        if ([_controllers count]>0) {
            for (int i=0; i<=_activeTab; i++) {
                UIView *titleView=[_titleViews objectAtIndex:i];
                titleView.transform=CGAffineTransformMakeRotation(-M_PI_2);
                titleView.frame=CGRectMake(i*ACORDEON_TITLE_SIZE, 0, ACORDEON_TITLE_SIZE, self.view.bounds.size.height);
                titleView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin;
            }
            
            _currentView.transform=CGAffineTransformIdentity;
            _currentView.frame=CGRectMake(ACORDEON_TITLE_SIZE*(_activeTab+1), 0, self.view.bounds.size.width-(ACORDEON_TITLE_SIZE*[_controllers count]), self.view.bounds.size.height);
            _currentView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
            
            int i=_activeTab+1;
            while (i<[_controllers count]) {
                UIView *titleView=[_titleViews objectAtIndex:i];
                titleView.transform=CGAffineTransformMakeRotation(-M_PI_2);
                titleView.frame=CGRectMake(self.view.bounds.size.width-(ACORDEON_TITLE_SIZE*([_controllers count]-i)), 0, ACORDEON_TITLE_SIZE, self.view.bounds.size.height);
                titleView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin;
                i++;
            }
        }
    }else{
        if ([_controllers count]>0) {
            for (int i=0; i<=_activeTab; i++) {
                UIView *titleView=[_titleViews objectAtIndex:i];
                titleView.transform=CGAffineTransformIdentity;
                titleView.frame=CGRectMake(0, i*ACORDEON_TITLE_SIZE, self.view.bounds.size.width,ACORDEON_TITLE_SIZE );
                titleView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
            }
            
            _currentView.transform=CGAffineTransformIdentity;
            _currentView.frame=CGRectMake(0, ACORDEON_TITLE_SIZE*(_activeTab+1), self.view.bounds.size.width,self.view.bounds.size.height-(ACORDEON_TITLE_SIZE*[_controllers count]));
            _currentView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
            
            int i=_activeTab+1;
            while (i<[_controllers count]) {
                UIView *titleView=[_titleViews objectAtIndex:i];
                titleView.transform=CGAffineTransformIdentity;
                titleView.frame=CGRectMake(0, self.view.bounds.size.height-(ACORDEON_TITLE_SIZE*([_controllers count]-i)), self.view.bounds.size.width,ACORDEON_TITLE_SIZE);
                titleView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
                i++;
            }
        }
    }
}

-(AcordeonDireccion)direccion{
    return _direccion;
}

-(void)setDireccion:(AcordeonDireccion)direccion{
    [self setDireccion:direccion animated:NO];
}

-(void)setDireccion:(AcordeonDireccion)direccion animated:(BOOL)animated{
    if (_direccion!=direccion) {
        _direccion=direccion;
        if (_direccion==AcordeonDireccionHorizontal) {
            _swipeIncreaseGR.direction=UISwipeGestureRecognizerDirectionLeft;
            _swipeDecreaseGR.direction=UISwipeGestureRecognizerDirectionRight;
        }else{
            _swipeIncreaseGR.direction=UISwipeGestureRecognizerDirectionUp;
            _swipeDecreaseGR.direction=UISwipeGestureRecognizerDirectionDown;
        }
        if ([self isViewLoaded]) {
            [UIView animateWithDuration:animated?ACORDEON_ANIMATION_DURATION:0 animations:^(){
                [self layoutForDirection:_direccion];
            }];
        }
    }
}


-(id)initWithViewControllers:(UIViewController *)firstVC, ...{
    self=[super init];
    if (self) {
        _activeTab=0;
        _animating=NO;
        _controllers=[NSMutableArray array];
        _titleViews=[NSMutableArray array];
        _direccion=AcordeonDireccionHorizontal;
        _tint=[UIColor blackColor];
        va_list args;
        va_start(args, firstVC);
        for (id arg = firstVC; arg != nil; arg = va_arg(args, UIViewController*))
        {
            if ([arg isKindOfClass:[UIViewController class]]) {
                [_controllers addObject:arg];
                [self addChildViewController:arg];
                [(UIViewController *)arg didMoveToParentViewController:self];
            }
        }
        va_end(args);
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    self.view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ACORDEON_TITLE_SIZE*[_controllers count] + ACORDEON_VIEW_MINSIZE, 100)];
    self.view.backgroundColor=[UIColor clearColor];
    self.view.clipsToBounds=YES;
    _swipeIncreaseGR=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swiped:)];
    _swipeIncreaseGR.delegate=self;
    _swipeDecreaseGR=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swiped:)];
    _swipeDecreaseGR.delegate=self;
    if (_direccion==AcordeonDireccionHorizontal) {
        _swipeIncreaseGR.direction=UISwipeGestureRecognizerDirectionLeft;
        _swipeDecreaseGR.direction=UISwipeGestureRecognizerDirectionRight;
    }else{
        _swipeIncreaseGR.direction=UISwipeGestureRecognizerDirectionUp;
        _swipeDecreaseGR.direction=UISwipeGestureRecognizerDirectionDown;
    }
    [self.view addGestureRecognizer:_swipeIncreaseGR];
    [self.view addGestureRecognizer:_swipeDecreaseGR];
    if ([_controllers count]>0) {
        for (int i=0; i<=_activeTab; i++) {
            //crear el view de título
            JAAcordeonBar *titleView=[[JAAcordeonBar alloc] initWithFrame:CGRectMake(0, i*ACORDEON_TITLE_SIZE, self.view.bounds.size.width, ACORDEON_TITLE_SIZE)];
            titleView.tint=_tint;
            titleView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
            titleView.backgroundColor=[UIColor colorWithHue:((i*1.0)/([_controllers count]*1.0)) saturation:1 brightness:0.5 alpha:1];
            JAAcordeonItem *item=[[JAAcordeonItem alloc] init];
            item.title=[(UIViewController *)[_controllers objectAtIndex:i] title];
            item.image=[self imageForViewController:[_controllers objectAtIndex:i]];
            titleView.item=item;
            UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTitleView:)];
            [titleView addGestureRecognizer:tapGR];
            [_titleViews addObject:titleView];
            [self.view addSubview:titleView];
        }
        //añadir el view del current VC
        
        _currentView = [[_controllers objectAtIndex:_activeTab] view];
        _currentView.frame=CGRectMake(0, ACORDEON_TITLE_SIZE*(_activeTab+1), self.view.bounds.size.width, self.view.bounds.size.height-(ACORDEON_TITLE_SIZE*[_controllers count]));
        _currentView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:_currentView];
        
        int i=_activeTab+1;
        while (i<[_controllers count]) {
            //crear el view de título
            JAAcordeonBar *titleView=[[JAAcordeonBar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-(ACORDEON_TITLE_SIZE*([_controllers count]-i)), self.view.bounds.size.width, ACORDEON_TITLE_SIZE)];
            titleView.tint=_tint;
            titleView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
            titleView.backgroundColor=[UIColor colorWithHue:((i*1.0)/([_controllers count]*1.0)) saturation:1 brightness:0.5 alpha:1];
            JAAcordeonItem *item=[[JAAcordeonItem alloc] init];
            item.title=[(UIViewController *)[_controllers objectAtIndex:i] title];
            item.image=[self imageForViewController:[_controllers objectAtIndex:i]];
            titleView.item=item;
            UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTitleView:)];
            [titleView addGestureRecognizer:tapGR];
            [_titleViews addObject:titleView];
            [self.view addSubview:titleView];
            i++;
        }
        [self layoutForDirection:_direccion];
    }
}


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [_titleViews removeAllObjects];
    _currentView=nil;
    _swipeDecreaseGR=nil;
    _swipeIncreaseGR=nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

-(int)viewControllersCount{
    return [_controllers count];
}

-(void)addViewController:(UIViewController *)newVC animated:(BOOL)animated{
    [self insertViewController:newVC atIndex:[_controllers count] animated:animated];
}

-(void)insertViewController:(UIViewController *)newVC atIndex:(int)index animated:(BOOL)animated{
    if (index<=[_controllers count]) {
        [_controllers insertObject:newVC atIndex:index];
        [self addChildViewController:newVC];
        if ([self isViewLoaded]) {
            if (index>_activeTab) {
                //crear el view de título
                JAAcordeonBar *titleView=[[JAAcordeonBar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-(ACORDEON_TITLE_SIZE*([_controllers count]-index-1)), self.view.bounds.size.width,ACORDEON_TITLE_SIZE)];
                titleView.tint=_tint;
                titleView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
                titleView.backgroundColor=[UIColor colorWithHue:((index*1.0)/([_controllers count]*1.0)) saturation:1 brightness:0.5 alpha:1];
                JAAcordeonItem *item=[[JAAcordeonItem alloc] init];
                item.title=[newVC title];
                item.image=[self imageForViewController:newVC];
                titleView.item=item;
                UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTitleView:)];
                [titleView addGestureRecognizer:tapGR];
                if (index==[_controllers count]-1) {
                    [self.view addSubview:titleView];
                    [self.view bringSubviewToFront:titleView];
                }else{
                    [self.view insertSubview:titleView belowSubview:[_titleViews objectAtIndex:index]];
                }
                [_titleViews insertObject:titleView atIndex:index];
                if (_direccion==AcordeonDireccionHorizontal) {
                    titleView.transform=CGAffineTransformMakeRotation(-M_PI_2);
                    titleView.frame=CGRectMake(self.view.bounds.size.width-(ACORDEON_TITLE_SIZE*([_controllers count]-index-1)),0, ACORDEON_TITLE_SIZE,self.view.bounds.size.height);
                }
            }else{
                _activeTab++;
                //crear el view de título
                JAAcordeonBar *titleView=[[JAAcordeonBar alloc] initWithFrame:CGRectMake(0,index*ACORDEON_TITLE_SIZE, self.view.bounds.size.width,ACORDEON_TITLE_SIZE)];
                titleView.tint=_tint;
                titleView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
                titleView.backgroundColor=[UIColor colorWithHue:((index*1.0)/([_controllers count]*1.0)) saturation:1 brightness:0.5 alpha:1];
                JAAcordeonItem *item=[[JAAcordeonItem alloc] init];
                item.title=[newVC title];
                item.image=[self imageForViewController:newVC];
                titleView.item=item;
                UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTitleView:)];
                [titleView addGestureRecognizer:tapGR];
                [self.view insertSubview:titleView belowSubview:[_titleViews objectAtIndex:index]];
                [_titleViews insertObject:titleView atIndex:index];
                if (_direccion==AcordeonDireccionHorizontal) {
                    titleView.transform=CGAffineTransformMakeRotation(-M_PI_2);
                    titleView.frame=CGRectMake(index*ACORDEON_TITLE_SIZE,0,ACORDEON_TITLE_SIZE, self.view.bounds.size.height);
                }
            }
            [UIView animateWithDuration:animated?ACORDEON_ANIMATION_DURATION:0 animations:^(){
                [self layoutForDirection:_direccion];
            } completion:^(BOOL finished){
                [newVC didMoveToParentViewController:self];
            }];
        }else{
            [newVC didMoveToParentViewController:self];
        }
    }
}

-(void)removeViewControllerAtIndex:(int)index animated:(BOOL)animated{    
    if (index!=_activeTab && index<[_controllers count]) {
        if (index<_activeTab) {
            _activeTab--;
        }
        UIViewController *removingVC=[_controllers objectAtIndex:index];
        [_controllers removeObjectAtIndex:index];
        [removingVC willMoveToParentViewController:nil];
        if ([self isViewLoaded]) {
            UIView *titleView=[_titleViews objectAtIndex:index];
            [_titleViews removeObjectAtIndex:index];
            [UIView animateWithDuration:animated?ACORDEON_ANIMATION_DURATION:0 animations:^(){
                [self layoutForDirection:_direccion];
                if (index>_activeTab) {
                    titleView.transform=CGAffineTransformTranslate(titleView.transform, 0, ACORDEON_TITLE_SIZE);
                }
            } completion:^(BOOL finished){
                [titleView removeFromSuperview];
                [removingVC removeFromParentViewController];
            }];
        }else{
            [removingVC removeFromParentViewController];
        }
    }else{
        //maybe some day
    }
}

-(UIViewController *)viewControllerAtIndex:(int)index{
    if (index<0 || index>=[_controllers count]) {
        return nil;
    }
    return [_controllers objectAtIndex:index];
}

-(void)activateViewControllerAtIndex:(int)index animated:(BOOL)animated{
    if (index>=0 && index<[_controllers count] && index!=_activeTab && !_animating && 
        (![self.delegate respondsToSelector:@selector(jaacordeonViewController:shouldActivateViewController:)] ||
         ([self.delegate respondsToSelector:@selector(jaacordeonViewController:shouldActivateViewController:)] && [self.delegate jaacordeonViewController:self shouldActivateViewController:[_controllers objectAtIndex:index]]))) {
            if ([self isViewLoaded]) {
                //activar el tab
                UIView *nextView=[[_controllers objectAtIndex:index] view];
                if ([self.delegate respondsToSelector:@selector(jaacordeonViewController:willActivateViewController:)]) {
                    [self.delegate jaacordeonViewController:self willActivateViewController:[_controllers objectAtIndex:index]];
                }
                if (_direccion==AcordeonDireccionHorizontal) {
                    nextView.frame=CGRectMake([[_titleViews objectAtIndex:index] frame].origin.x + ACORDEON_TITLE_SIZE, 0, self.view.bounds.size.width-(ACORDEON_TITLE_SIZE*[_controllers count]), self.view.bounds.size.height);
                }else{
                    nextView.frame=CGRectMake(0, [[_titleViews objectAtIndex:index] frame].origin.y + ACORDEON_TITLE_SIZE, self.view.bounds.size.width, self.view.bounds.size.height-(ACORDEON_TITLE_SIZE*[_controllers count]));
                }
                nextView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
                
                
                [self transitionFromViewController:[_controllers objectAtIndex:_activeTab] toViewController:[_controllers objectAtIndex:index] duration:animated?ACORDEON_ANIMATION_DURATION:0 options:0 animations:^(){

                    if (index+1<[_titleViews count]) {
                        [self.view insertSubview:nextView belowSubview:[_titleViews objectAtIndex:index+1]];
                    }else{
                        [self.view addSubview:nextView];
                    }

                    _animating=YES;
                    if (index<_activeTab) {
                        float desplazamiento=_currentView.frame.size.width;
                        if (_direccion==AcordeonDireccionHorizontal) {
                            _currentView.transform=CGAffineTransformTranslate(_currentView.transform,desplazamiento, 0);
                        }else{
                            desplazamiento=_currentView.frame.size.height;
                            _currentView.transform=CGAffineTransformTranslate(_currentView.transform,0,desplazamiento);
                        }
                        for (int i=index+1; i<=_activeTab; i++) {
                            ((UIView*)[_titleViews objectAtIndex:i]).transform=CGAffineTransformTranslate(((UIView*)[_titleViews objectAtIndex:i]).transform,0, desplazamiento);
                            ((UIView*)[_titleViews objectAtIndex:i]).autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin;
                        }
                    }else{
                        float desplazamiento=_currentView.frame.size.width;
                        if (_direccion==AcordeonDireccionHorizontal) {
                            nextView.transform=CGAffineTransformTranslate(nextView.transform,-desplazamiento, 0);
                        }else{
                            desplazamiento=_currentView.frame.size.height;
                            nextView.transform=CGAffineTransformTranslate(nextView.transform,0,-desplazamiento);
                        }
                        
                        for (int i=_activeTab+1; i<=index; i++) {
                            ((UIView*)[_titleViews objectAtIndex:i]).transform=CGAffineTransformTranslate(((UIView*)[_titleViews objectAtIndex:i]).transform,0, -desplazamiento);
                            ((UIView*)[_titleViews objectAtIndex:i]).autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin;
                        }
                    }
                } completion:^(BOOL finished){
                    _animating=NO;
                    [_currentView removeFromSuperview];
                    if ([self.delegate respondsToSelector:@selector(jaacordeonViewController:willActivateViewController:)]) {
                        [self.delegate jaacordeonViewController:self didActivateViewController:[_controllers objectAtIndex:index]];
                    }
                    _activeTab=index;
                    _currentView=nextView;
                    [self layoutForDirection:_direccion];
                }];
            }else{
                [self transitionFromViewController:[_controllers objectAtIndex:_activeTab] toViewController:[_controllers objectAtIndex:index] duration:0 options:0 animations:NULL completion:NULL];
                _activeTab=index;
            }
            
        }
}

-(void)activateViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [self activateViewControllerAtIndex:[_controllers indexOfObject:viewController] animated:animated];
}

-(void)tappedTitleView:(UITapGestureRecognizer *)tapGR{
    [self activateViewControllerAtIndex:[_titleViews indexOfObject:tapGR.view] animated:YES];
}

-(UIImage *)imageForViewController:(UIViewController *)viewController{
    UIImage *result=nil;
    
    if ([self.delegate respondsToSelector:@selector(jaacordeonViewController:imageForViewController:)]) {
        result=[self.delegate jaacordeonViewController:self imageForViewController:viewController];
    }
    
    return result;
}

#pragma mark - swipe

-(void)swiped:(UISwipeGestureRecognizer *)swipeGR{
    int swipedIndex=0;//[_titleViews indexOfObject:swipeGR];
    for (; swipedIndex<[_titleViews count]; swipedIndex++) {
        CGPoint location=[swipeGR locationInView:[_titleViews objectAtIndex:swipedIndex]];
        if (location.x<0 || location.y<0) {
            break;
        }
    }
    swipedIndex--;
    if (swipeGR==_swipeDecreaseGR) {
        swipedIndex--;
    }
    [self activateViewControllerAtIndex:swipedIndex animated:YES];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return NO;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    int index=[_titleViews indexOfObject:touch.view];
    return ([touch.view isKindOfClass:[JAAcordeonBar class]] && 
            ((gestureRecognizer==_swipeDecreaseGR && index<=_activeTab) || (gestureRecognizer==_swipeIncreaseGR && index>_activeTab))
            );
}

@end





@implementation JAAcordeonItem

@synthesize image=_image;
@synthesize title=_title;

@end



@interface JAAcordeonBar(){
    UILabel *_titleLabel;
    UIImageView *_imageView;
}

@end

@implementation JAAcordeonBar

@synthesize item=_item;
@synthesize tint=_tint;

-(void)setTint:(UIColor *)tint{
    if (tint!=_tint) {
        _tint=tint;
        [self setNeedsDisplay];
    }
}

-(id)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        _tint=[UIColor lightGrayColor];
        //crear la imageview y el label
        _titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(ACORDEON_TITLE_MARGIN_2+ACORDEON_TITLE_IMAGE_SIDE, ACORDEON_TITLE_MARGIN, self.bounds.size.width-ACORDEON_TITLE_MARGIN_2, self.bounds.size.height-ACORDEON_TITLE_MARGIN_2)];
        _titleLabel.text=@"";
        _titleLabel.textAlignment=UITextAlignmentLeft;
        _titleLabel.textColor=[UIColor whiteColor];
        _titleLabel.shadowColor=[UIColor darkGrayColor];
        _titleLabel.shadowOffset=CGSizeMake(1, 1);
        _titleLabel.backgroundColor=[UIColor clearColor];
        _titleLabel.font=[UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
        _titleLabel.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _titleLabel.userInteractionEnabled=NO;
        [self addSubview:_titleLabel];
        _imageView=[[UIImageView alloc] initWithFrame:CGRectMake(ACORDEON_TITLE_MARGIN, (self.bounds.size.height-ACORDEON_TITLE_IMAGE_SIDE)/2.0, ACORDEON_TITLE_IMAGE_SIDE, ACORDEON_TITLE_IMAGE_SIDE)];
        _imageView.opaque=NO;
        _imageView.backgroundColor=[UIColor clearColor];
        _imageView.contentMode=UIViewContentModeCenter;
        _imageView.userInteractionEnabled=NO;
        [self addSubview:_imageView];
    }
    
    return self;
}

-(void)setItem:(JAAcordeonItem *)item{
    if (item!=_item) {
        [_item removeObserver:self forKeyPath:@"title"];
        [_item removeObserver:self forKeyPath:@"image"];
        _item=item;
        _titleLabel.text=_item.title;
        _imageView.image=_item.image;
        [_item addObserver:self forKeyPath:@"image" options:0 context:NULL];
        [_item addObserver:self forKeyPath:@"title" options:0 context:NULL];
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object==_item) {
        if ([keyPath isEqualToString:@"title"]) {
            _titleLabel.text=_item.title;
        }else if ([keyPath isEqualToString:@"image"]){
            _imageView.image=_item.image;
        }
    }
}

-(void)drawRect:(CGRect)rect{
    
    CGFloat hue, saturation, brightness;
    [_tint getHue:&hue saturation:&saturation brightness:&brightness alpha:NULL];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    
    CGFloat locations[3];
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    NSMutableArray *colors = [NSMutableArray arrayWithCapacity:3];
    UIColor *color = [UIColor colorWithHue:hue saturation:1*saturation brightness:0.9*(brightness+0.3) alpha:1];
    [colors addObject:(id)[color CGColor]];
    locations[0] = 0.0;
    color = [UIColor colorWithHue:hue saturation:1*saturation brightness:0.5*(brightness+0.2) alpha:1];
    [colors addObject:(id)[color CGColor]];
    locations[1] =  0.66;
    color = [UIColor colorWithHue:hue saturation:0.6*saturation brightness:0.4*brightness alpha:1];
    [colors addObject:(id)[color CGColor]];
    locations[2] = 1.0;  
    
    CGGradientRef gradient = CGGradientCreateWithColors(space, (__bridge CFArrayRef)colors, locations);
    CGColorSpaceRelease(space);
    
    CGPoint point2 = CGPointMake((self.bounds.size.width / 2.0), self.bounds.size.height - 0.5f);
	CGPoint point = CGPointMake((self.bounds.size.width / 2.0), 0.0);
	CGContextDrawLinearGradient(context, gradient, point, point2, (kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation));
    
    CGPoint linePoints[2];
    linePoints[0]=CGPointMake(0, self.bounds.size.height);
    linePoints[1]=CGPointMake(self.bounds.size.width, self.bounds.size.height);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithHue:hue saturation:0.2*saturation brightness:0.3*brightness alpha:1].CGColor);
    CGContextStrokeLineSegments(context, linePoints, 2);
    
    CGGradientRelease(gradient);
    
    CGContextSetLineWidth(context, 1);
    
    [[UIColor colorWithHue:0 saturation:0 brightness:0 alpha:0.7] setStroke];
    
    CGPoint points[2];
    points[0] = CGPointMake(0, 0.5);
    points[1] = CGPointMake(rect.size.width, 0.5);
    
    CGContextStrokeLineSegments(context, points, 2);
    
    [[UIColor colorWithHue:0 saturation:0 brightness:0.7+(0.3*brightness) alpha:0.4] setStroke];
    points[0] = CGPointMake(0, 1.5);
    points[1] = CGPointMake(rect.size.width, 1.5);
    
    CGContextStrokeLineSegments(context, points, 2);
    
    CGContextRestoreGState(context);
}

-(void)dealloc{
    [_item removeObserver:self forKeyPath:@"title"];
    [_item removeObserver:self forKeyPath:@"image"];
}

@end

