//
//  tresVC.m
//
//  Created by Javier Alonso Gutierrez on 14/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "tresVC.h"

@implementation tresVC
@synthesize titleLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)setTitle:(NSString *)title{
    [super setTitle:title];
    if ([self isViewLoaded]) {
        self.titleLabel.text=title;
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.titleLabel.text=self.title;
    NSLog(@"%@ didLoad",self.title);
}

- (void)viewDidUnload
{
    [self setTitleLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    NSLog(@"%@ didUnload",self.title);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"%@ will appear",self.title);
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"%@ will disappear",self.title);
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"%@ did appear",self.title);
}

-(void)viewDidDisappear:(BOOL)animated{
    NSLog(@"%@ did disappear",self.title);
}

@end
