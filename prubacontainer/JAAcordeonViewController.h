//
//  AcordeonViewController.h
//
//  Created by Javier Alonso Gutierrez on 14/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    AcordeonDireccionHorizontal,
    AcordeonDireccionVertical
} AcordeonDireccion;


@protocol JAAcordeonViewControllerDelegate;


@interface JAAcordeonViewController : UIViewController

@property (nonatomic) AcordeonDireccion direccion;
@property (nonatomic, weak) id<JAAcordeonViewControllerDelegate> delegate;
@property (nonatomic, strong) UIColor *tint;

-(id)initWithViewControllers:(UIViewController *)firstVC, ... NS_REQUIRES_NIL_TERMINATION;

-(int)viewControllersCount;
-(void)addViewController:(UIViewController *)newVC animated:(BOOL)animated;
-(void)insertViewController:(UIViewController *)newVC atIndex:(int)index animated:(BOOL)animated;
-(void)removeViewControllerAtIndex:(int)index animated:(BOOL)animated;
-(UIViewController *)viewControllerAtIndex:(int)index;
-(void)activateViewControllerAtIndex:(int)index animated:(BOOL)animated;

-(void)setDireccion:(AcordeonDireccion)direccion animated:(BOOL)animated;

@end



@protocol JAAcordeonViewControllerDelegate <NSObject>

@optional

-(BOOL)jaacordeonViewController:(JAAcordeonViewController *)acordeonViewController shouldActivateViewController:(UIViewController *)viewController;
-(void)jaacordeonViewController:(JAAcordeonViewController *)acordeonViewController willActivateViewController:(UIViewController *)viewController;
-(void)jaacordeonViewController:(JAAcordeonViewController *)acordeonViewController didActivateViewController:(UIViewController *)viewController;
-(UIImage *)jaacordeonViewController:(JAAcordeonViewController *)acordeonViewController imageForViewController:(UIViewController *)viewController;

@end


@interface JAAcordeonItem : NSObject

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *title;

@end


@interface JAAcordeonBar : UIView

@property (nonatomic, strong) JAAcordeonItem *item;
@property (nonatomic, strong) UIColor *tint;

@end
